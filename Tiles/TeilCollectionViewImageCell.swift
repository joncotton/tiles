//
//  TeilCollectionViewImageCell.swift
//  Tiles
//
//  Created by Cotton, Jonathan (Mobile Developer) on 03/02/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit

class TileCollectionViewImageCell<TileViewType where TileViewType: ImageTileView>: TileCollectionViewCell<TileViewType> {
    func redrawTileWithContent(tileContent: TileContent, imageName: String) {
        super.redrawTileWithContent(tileContent)
        
        renderTileImageWithImageNamed(imageName)
    }
    
    private func renderTileImageWithImageNamed(imageName: String) {
        tileView.imageView.image = UIImage(named: imageName)
    }
}