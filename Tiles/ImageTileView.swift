//
//  ImageTile.swift
//  Tiles
//
//  Created by Cotton, Jonathan (Mobile Developer) on 02/02/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit

class ImageTileView: TileView {
    let imageView = UIImageView()
    let animationTimings = []
    
    lazy var movementRange: CGFloat = {
        var rand = arc4random_uniform(5)
        if rand < 1 { rand = 1 }
        return CGFloat((rand * 10) / 2)
    }()
    
    lazy var animationDuration: Double = {
        var rand = arc4random_uniform(5)
        if rand < 1 { rand = 1 }
        return Double(rand * 20)
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupImageView()
        applyMotionEffect()
        applyAnimations()
        
        clipsToBounds = true
        titleLabel.backgroundColor = UIColor.blackColor()
        backgroundColor = UIColor.clearColor()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func applyMotionEffect() {
        let horizontalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.x", type: .TiltAlongHorizontalAxis)
        horizontalMotionEffect.minimumRelativeValue = -movementRange
        horizontalMotionEffect.maximumRelativeValue = movementRange
        
        let verticalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.y", type: .TiltAlongVerticalAxis)
        verticalMotionEffect.minimumRelativeValue = -movementRange
        verticalMotionEffect.maximumRelativeValue = movementRange
        
        let motionEffects = UIMotionEffectGroup()
        motionEffects.motionEffects = [horizontalMotionEffect, verticalMotionEffect]
        imageView.addMotionEffect(motionEffects)
    }
    
    func applyAnimations() {
        let xAnimation = CAKeyframeAnimation(keyPath: "position.x")
        xAnimation.values = [0, movementRange, 0]
        xAnimation.keyTimes = [0, 0.5, 1]
        xAnimation.duration = animationDuration
        xAnimation.additive = true
        xAnimation.repeatCount = .infinity
        xAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        imageView.layer.addAnimation(xAnimation, forKey: "x")
        
        let yAnimation = CAKeyframeAnimation(keyPath: "position.y")
        yAnimation.values = [0, movementRange, 0]
        yAnimation.keyTimes = [0, 0.5, 1]
        yAnimation.duration = animationDuration
        yAnimation.additive = true
        yAnimation.repeatCount = .infinity
        yAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        imageView.layer.addAnimation(yAnimation, forKey: "y")
        
        let zoomAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        zoomAnimation.duration = animationDuration
        zoomAnimation.repeatCount = .infinity
        zoomAnimation.values = [1, 0.99, 0.94, 0.91, 0.9, 0.91, 0.94, 0.99, 1]
        zoomAnimation.keyTimes = [0, 0.1, 0.3, 0.4, 0.5, 0.6, 0.7, 0.9, 1]
        imageView.layer.addAnimation(zoomAnimation, forKey: "zoom")
    }
    
    private func setupImageView() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        insertSubview(imageView, atIndex: 0)
        
        let widthConstraint = NSLayoutConstraint(item: imageView, attribute: .Width, relatedBy: .Equal, toItem: self, attribute: .Width, multiplier: 1.1, constant: (movementRange * 2))
        let heightConstraint = NSLayoutConstraint(item: imageView, attribute: .Height, relatedBy: .Equal, toItem: self, attribute: .Height, multiplier: 1.1, constant: (movementRange * 2))
        let xConstraint = NSLayoutConstraint(item: imageView, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1.0, constant: 0.0)
        let yConstraint = NSLayoutConstraint(item: imageView, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1.0, constant: 0.0)
        
        addConstraints([widthConstraint, heightConstraint,xConstraint, yConstraint])
        
        imageView.contentMode = .ScaleAspectFill
    }
    
}
