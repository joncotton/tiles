//
//  CellMatrix.swift
//  Tiles
//
//  Created by Cotton, Jonathan (Mobile Developer) on 02/02/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit

struct CellMatrix {
    let numberOfColumns: Int
    let columnWidth: CGFloat
    private var matrix: [[Bool]]

    private var newRow: [Bool] {
        return [Bool](count: numberOfColumns, repeatedValue: false)
    }
    
    init(numberOfColumns: Int, columnWidth: CGFloat) {
        self.numberOfColumns = numberOfColumns
        self.columnWidth = columnWidth
        
        let row = [Bool](count: numberOfColumns, repeatedValue: false)
        self.matrix = [row]
    }
    
    mutating func rectForTileOfShape(tileShape: TileShape) -> CGRect {
        let size = cellSizeForTileShape(tileShape)
        var cellRect: CellRect?
        
        rowLoop: for (row, columns) in matrix.enumerate() {
            columnLoop: for (column, isOccupied) in columns.enumerate() {
                if !isOccupied {
                    let gapOrigin = CellPoint(column: column, row: row)
                    let gapSize = sizeOfGapStartingAtPoint(gapOrigin)
                    
                    if size.width <= gapSize.width && size.height <= gapSize.height {
                        for currentRow in 0 ..< size.height {
                            if row + currentRow >= matrix.count {
                                matrix.append(newRow)
                            }
                            
                            for currentColumn in 0 ..< size.width {
                                assert(matrix[row + currentRow][column + currentColumn] == false)
                                matrix[row + currentRow][column + currentColumn] = true
                            }
                        }
                        
                        cellRect = CellRect(origin: gapOrigin, size: size)
                        break rowLoop
                    }
                }
            }
        }
        
        if cellRect == nil {
            let origin = CellPoint(column: 0, row: matrix.count)
            addRowsForCellOfSize(size)
            cellRect = CellRect(origin: origin, size: size)
        }
        
        let realRect = cellRect!.realRectForBaseSize(columnWidth)
        return realRect
    }
    
    private func cellSizeForTileShape(tileShape: TileShape) -> CellSize {
        switch tileShape {
        case .smallSquare:
            return CellSize(width: 1, height: 1)
            
        case .largeSquare:
            return CellSize(width: 2, height: 2)
            
        case .smallRectangle:
            return CellSize(width: 2, height: 1)
            
        case .largeRectangle:
            return CellSize(width: 4, height: 2)
            
        case .largeShortRectangle:
            return CellSize(width: 3, height: 2)
        }
    }
    
    private func sizeOfGapStartingAtPoint(point: CellPoint) -> CellSize {
        var currentColumn = point.column
        var currentRow = point.row
        var maxColumn = point.column
        var hitBottom = false
        var hasMovedDown = false
        
        // TODO: this dunt werk
        while matrix[currentRow][currentColumn] == false {
            currentColumn = currentColumn + 1

            if currentColumn >= numberOfColumns || matrix[currentRow][currentColumn] == true {
                let nextRow = currentRow + 1
                currentColumn = point.column
                
                if nextRow >= matrix.count {
                    hitBottom = true
                    break
                }
                
                if matrix[nextRow][currentColumn] == true {
                    break
                }
                
                currentRow = nextRow
                hasMovedDown = true
            } else {
                if !hasMovedDown {
                    maxColumn = max(maxColumn, currentColumn)
                }
            }
        }
        
        let gapWidth = (maxColumn - point.column) + 1
        var gapHeight = (currentRow - point.row) + 1
        if hitBottom {
            // if we got to the bottom row, just say the gap is really tall so the cell will fit
            gapHeight = 9999
        }
        
        return CellSize(width: gapWidth, height: gapHeight)
    }
    
    mutating private func addRowsForCellOfSize(size: CellSize) {
        var i = 0
        while i < size.height {
            var row = newRow
            for column in 0 ..< size.width {
                row[column] = true
            }
            matrix.append(row)
            i++
        }
    }
    
}

private struct CellSize {
    let width: Int
    let height: Int
    
    init(width: Int, height: Int) {
        self.width = width
        self.height = height
    }
    
    func realSizeForBaseSize(baseSize: CGFloat) -> CGSize {
        let realWidth = CGFloat(width) * baseSize
        let realHeight = CGFloat(height) * baseSize
        
        return CGSizeMake(realWidth, realHeight)
    }
}

private struct CellPoint {
    let column: Int
    let row: Int
    
    init(column: Int, row: Int) {
        self.column = column
        self.row = row
    }
    
    func realPointForBaseSize(baseSize: CGFloat) -> CGPoint {
        let x = CGFloat(column) * baseSize
        let y = CGFloat(row) * baseSize
        
        return CGPointMake(x, y)
    }
}

private struct CellRect {
    let origin: CellPoint
    let size: CellSize
    
    init(column: Int, row: Int, width: Int, height: Int) {
        let origin = CellPoint(column: column, row: row)
        let size = CellSize(width: width, height: height)
        
        self = CellRect(origin: origin, size: size)
    }
    
    init(origin: CellPoint, size: CellSize) {
        self.origin = origin
        self.size = size
    }
    
    func realRectForBaseSize(baseSize: CGFloat) -> CGRect {
        let realOrigin = origin.realPointForBaseSize(baseSize)
        let realSize = size.realSizeForBaseSize(baseSize)
        
        return CGRect(origin: realOrigin, size: realSize)
    }
}