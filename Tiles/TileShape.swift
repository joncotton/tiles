//
//  TileShape.swift
//  Tiles
//
//  Created by Cotton, Jonathan (Mobile Developer) on 31/01/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit

enum TileShape {
    case smallSquare
    case largeSquare
    case smallRectangle
    case largeRectangle
    case largeShortRectangle
}