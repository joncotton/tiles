//
//  UIColor.swift
//  Tiles
//
//  Created by Cotton, Jonathan (Mobile Developer) on 02/02/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit

extension UIColor {
    static func skyBlue() -> UIColor {
        return UIColor(red: 0.0, green: 115.0/255.0, blue: 197.0/225.0, alpha: 1.0)
    }
}