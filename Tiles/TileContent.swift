//
//  TileContent.swift
//  Tiles
//
//  Created by Cotton, Jonathan (Mobile Developer) on 31/01/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit

struct TileContent {
    let title: String
    let action: () -> ()
    var icon: Character?
    var logoName: String?
    
    init(title: String, action: () -> ()) {
        self.title = title
        self.action = action
    }
}
