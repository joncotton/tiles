//
//  Constants.swift
//  Tiles
//
//  Created by Cotton, Jonathan (Mobile Developer) on 31/01/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit

let TileSpacing = CGFloat(2.0)
let BaseTileWidth = CGFloat(80.0)