//
//  TileCollectionViewController.swift
//  Tiles
//
//  Created by Cotton, Jonathan (Mobile Developer) on 31/01/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit

private var tiles: [Tile] = [
    Tile(shape: .largeRectangle, background: .image(path: "tile1"), content: TileContent(title: "Sky Broadband") {print("Action!")}),
    Tile(shape: .largeSquare, background: .solidColour(colour: UIColor.skyBlue()), content: TileContent(title: "Your Bill") {print("Action!")}),
    Tile(shape: .smallSquare, background: .solidColour(colour: UIColor.skyBlue()), content: TileContent(title: "Settings") {print("Action!")}),
    Tile(shape: .smallSquare, background: .solidColour(colour: UIColor.skyBlue()), content: TileContent(title: "Profile") {print("Action!")}),
    Tile(shape: .largeSquare, background: .image(path: "tile2"), content: TileContent(title: "Movies") {print("Action!")}),
    Tile(shape: .smallRectangle, background: .solidColour(colour: UIColor.skyBlue()), content: TileContent(title: "Contact Us") {print("Action!")}),
    Tile(shape: .smallRectangle, background: .solidColour(colour: UIColor.skyBlue()), content: TileContent(title: "Help") {print("Action!")}),
    Tile(shape: .smallRectangle, background: .solidColour(colour: UIColor.skyBlue()), content: TileContent(title: "Sky Store") {print("Action!")}),
    Tile(shape: .largeShortRectangle, background: .image(path: "tile4"), content: TileContent(title: "Box Sets") {print("Action!")}),
    Tile(shape: .largeRectangle, background: .video(path: "video1"), content: TileContent(title: "Sky Movies Offer") {print("Action!")})
]

private func generateTile(label: String) -> Tile {
    let rand = arc4random_uniform(6)
    var shape: TileShape
    switch rand {
    case 1:
        shape = .smallSquare
        
    case 2:
        shape = .smallRectangle
        
    case 3:
        shape = .largeRectangle
        
    case 4:
        shape = .largeSquare
        
    case 5:
        shape = .largeShortRectangle
        
    default:
        shape = .smallSquare
    }
    
    let tileColour = TileBackground.solidColour(colour: UIColor.skyBlue())

    return Tile(shape: shape, background: tileColour, content: TileContent(title: label) {
        print("Tile Action")
    })
}

class TileCollectionViewController: UICollectionViewController {
    private struct ReuseIdentifiers {
        static let tile = String(TileCollectionViewCell<TileView>.self)
        static let image = String(TileCollectionViewImageCell<ImageTileView>.self)
        static let video = String(TileCollectionViewVideoCell<VideoTileView>.self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for i in 0 ..< 51 {
            tiles.append(generateTile("\(i)"))
        }

        self.collectionView!.registerClass(TileCollectionViewCell<TileView>.self, forCellWithReuseIdentifier: ReuseIdentifiers.tile)
        self.collectionView!.registerClass(TileCollectionViewImageCell<ImageTileView>.self, forCellWithReuseIdentifier: ReuseIdentifiers.image)
        self.collectionView!.registerClass(TileCollectionViewVideoCell<VideoTileView>.self, forCellWithReuseIdentifier: ReuseIdentifiers.video)

        if let layout = collectionView?.collectionViewLayout as? TileCollectionViewLayout {
            layout.delegate = self
        }
        
        collectionView!.contentInset = UIEdgeInsetsMake(20.0, 0.0, 0.0, 0.0)
    }

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tiles.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let tile = tiles[indexPath.row]
        var cell: UICollectionViewCell!
        
        switch tile.background {
        case .solidColour(let colour):
            let reuseIdentifier = ReuseIdentifiers.tile
            if let tileCell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as? TileCollectionViewCell<TileView> {
                tileCell.redrawTileWithContent(tile.content, colour: colour)
                cell = tileCell
            }
            
        case .autoGradient(_):
            break
            
        case .gradient(_, _):
            break
            
        case .image(let imageName):
            let reuseIdentifier = ReuseIdentifiers.image
            if let imageCell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as? TileCollectionViewImageCell<ImageTileView> {
                imageCell.redrawTileWithContent(tile.content, imageName: imageName)
                cell = imageCell
            }
        
        case .video(let videoName):
            let reuseIdentifier = ReuseIdentifiers.video
            if let videoCell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as? TileCollectionViewVideoCell<VideoTileView> {
                videoCell.redrawTileWithContent(tile.content, videoName: videoName)
                cell = videoCell
            }
            
        case .bill(_, _):
            break
            
        }
        
        return cell
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        if let layout = collectionView?.collectionViewLayout as? TileCollectionViewLayout {
            layout.clearCache()
        }
    }

}

extension TileCollectionViewController: TileLayoutDelegate {
    func collectionView(collectionView:UICollectionView, contentForTileAtIndexPath indexPath:NSIndexPath) -> Tile {
        return tiles[indexPath.row]
    }
}
