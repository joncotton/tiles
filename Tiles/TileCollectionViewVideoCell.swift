//
//  TileCollectionViewVideoCell.swift
//  Tiles
//
//  Created by Cotton, Jonathan (Mobile Developer) on 03/02/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation
import AVFoundation

class TileCollectionViewVideoCell<TileViewType where TileViewType: VideoTileView>: TileCollectionViewCell<TileViewType> {
    var videoName: String?
    var avItem: AVPlayerItem?
    
    func redrawTileWithContent(tileContent: TileContent, videoName: String) {
        super.redrawTileWithContent(tileContent)
        
        renderTileVideoWithVideoNamed(videoName)
    }
    
    private func renderTileVideoWithVideoNamed(videoName: String) {
        if self.videoName != videoName {
            let fullPath = NSBundle.mainBundle().pathForResource(videoName, ofType: "mp4")
            let URL = NSURL(fileURLWithPath: fullPath!)
            avItem = AVPlayerItem(URL: URL)
            tileView.player!.replaceCurrentItemWithPlayerItem(avItem!)
            tileView.player!.muted = true
            tileView.player!.play()

        }
        
        self.videoName = videoName
        
    }
}