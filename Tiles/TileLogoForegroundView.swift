//
//  TileLogoForegroundView.swift
//  Tiles
//
//  Created by Cotton, Jonathan (Mobile Developer) on 03/02/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit

class TileLogoForegroundView: UIView {
    let logo: UIImage
    
    init(logoName: String) {
        self.logo = UIImage(named: logoName) ?? UIImage()
        
        super.init(frame: CGRectZero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
