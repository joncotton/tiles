//
//  Tile.swift
//  Tiles
//
//  Created by Cotton, Jonathan (Mobile Developer) on 31/01/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit

class TileView: UIView {
    let titleLabel = UILabel()
    var logoView = UIImageView() // hidden by default
    var iconLabel = UILabel() // hidden by default
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupTitle()
        setupIcon()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupTitle() {
        logoView.hidden = true
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        logoView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(titleLabel)
        addSubview(logoView)
        
        let views = ["label": titleLabel, "logo": logoView]
        let hConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-[logo]-0-[label]", options: .AlignAllCenterY, metrics: nil, views: views)
        let vLabelConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[label]-|", options: .AlignAllLeft, metrics: nil, views: views)
        let vLogoConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[logo]-|", options: .AlignAllLeft, metrics: nil, views: views)
        
        addConstraints(hConstraints)
        addConstraints(vLabelConstraints)
        addConstraints(vLogoConstraints)
        
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.font = UIFont(name: "Sky Regular", size: 16.0)
    }
    
    func setupIcon() {
        iconLabel.hidden = true
        iconLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(iconLabel)
        
        let xConstraint = NSLayoutConstraint(item: iconLabel, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1.0, constant: 0.0)
        let yConstraint = NSLayoutConstraint(item: iconLabel, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1.0, constant: 0.0)
        
        addConstraints([xConstraint, yConstraint])
        
        iconLabel.textColor = UIColor.whiteColor()
        iconLabel.font = UIFont(name: "Sky Regular", size: 16.0)
    }
}
