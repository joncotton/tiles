//
//  TileBillForegroundView.swift
//  Tiles
//
//  Created by Cotton, Jonathan (Mobile Developer) on 03/02/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit

class TileBillForegroundView: UIView {
    let currency: Character
    let amount: Float
    
    init(currency: Character, amount: Float) {
        self.currency = currency
        self.amount = amount
        
        super.init(frame: CGRectZero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
