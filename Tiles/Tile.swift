//
//  Tile.swift
//  Tiles
//
//  Created by Cotton, Jonathan (Mobile Developer) on 04/02/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation

struct Tile {
    let shape: TileShape
    let background: TileBackground
    let content: TileContent
}