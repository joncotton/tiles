//
//  TileCollectionViewCell.swift
//  Tiles
//
//  Created by Cotton, Jonathan (Mobile Developer) on 31/01/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit
import AVFoundation

class TileCollectionViewCell<TileViewType: TileView>: UICollectionViewCell {
    let tileView: TileViewType
    
    override init(frame: CGRect) {
        tileView = TileViewType()
        
        super.init(frame: frame)
        
        setupTileView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func redrawTileWithContent(tileContent: TileContent, colour: UIColor) {
        redrawTileWithContent(tileContent)
        
        tileView.backgroundColor = colour
    }
    
    func redrawTileWithContent(tileContent: TileContent) {
        renderTileTitle(tileContent.title)
        
        if let icon = tileContent.icon {
            renderTileIcon(icon)
        }
        
        if let logoName = tileContent.logoName {
            renderTileLogo(logoName)
        }
    }
    
    private func setupTileView() {
        tileView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(tileView)
        
        let views = ["tile": tileView]
        let hConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-(\(TileSpacing))-[tile]-(\(TileSpacing))-|", options: .AlignAllLeft, metrics: nil, views: views)
        let vConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-(\(TileSpacing))-[tile]-(\(TileSpacing))-|", options: .AlignAllLeft, metrics: nil, views: views)
        
        contentView.addConstraints(hConstraints)
        contentView.addConstraints(vConstraints)
    }
    
    private func renderTileTitle(title: String) {
        tileView.titleLabel.text = title
    }
    
    private func renderTileIcon(icon: Character) {
        tileView.iconLabel.hidden = false
        tileView.iconLabel.text = String(icon)
    }
    
    private func renderTileLogo(logoName: String) {
        guard let logoImage = UIImage(named: logoName)
        else {
            print("Failed to load Image named: \(logoName)")
            return
        }
        
        tileView.logoView.hidden = false
        tileView.logoView.image = logoImage
    }

}
