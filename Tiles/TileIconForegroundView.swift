//
//  TileIconForegroundView.swift
//  Tiles
//
//  Created by Cotton, Jonathan (Mobile Developer) on 03/02/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit

class TileIconForegroundView: UIView {
    let icon: Character
    
    init(icon: Character) {
        self.icon = icon
        
        super.init(frame: CGRectZero)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
