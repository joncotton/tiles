//
//  TileCollectionViewLayout.swift
//  Tiles
//
//  Created by Cotton, Jonathan (Mobile Developer) on 31/01/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit

protocol TileLayoutDelegate {
    func collectionView(collectionView:UICollectionView, contentForTileAtIndexPath indexPath:NSIndexPath) -> Tile
}

class TileCollectionViewLayout: UICollectionViewLayout {    
    private let columnWidth = CGFloat(BaseTileWidth)
    private var contentHeight: CGFloat = 0.0
    private var cache = [UICollectionViewLayoutAttributes]()
    private var cellMatrix: CellMatrix?
    
    var delegate: TileLayoutDelegate?

    private var contentWidth: CGFloat {
        let insets = collectionView!.contentInset
        return CGRectGetWidth(collectionView!.bounds) - (insets.left + insets.right)
    }
    
    override func prepareLayout() {
        super.prepareLayout()
        
        if cache.isEmpty {
            let numberOfColumns = Int(contentWidth / BaseTileWidth)
            cellMatrix = CellMatrix(numberOfColumns: numberOfColumns, columnWidth: columnWidth)
            
            for item in 0 ..< collectionView!.numberOfItemsInSection(0) {
                let indexPath = NSIndexPath(forItem: item, inSection: 0)
 
                let tile = delegate!.collectionView(collectionView!, contentForTileAtIndexPath: indexPath)
                let frame = cellMatrix!.rectForTileOfShape(tile.shape)
                
                // TODO: Check these are actually here
                let attributes = UICollectionViewLayoutAttributes(forCellWithIndexPath: indexPath)
                attributes.frame = frame
                cache.append(attributes)
                
                contentHeight = max(contentHeight, CGRectGetMaxY(frame))
            }
        }
    }
    
    override func collectionViewContentSize() -> CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    override func layoutAttributesForElementsInRect(rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes = [UICollectionViewLayoutAttributes]()
        
        for attributes in cache {
            if CGRectIntersectsRect(attributes.frame, rect) {
                layoutAttributes.append(attributes)
            }
        }

        return layoutAttributes
    }
    
    override func shouldInvalidateLayoutForBoundsChange(newBounds: CGRect) -> Bool {
        return true
    }
    
    func clearCache() {
        cache.removeAll()
    }
}
