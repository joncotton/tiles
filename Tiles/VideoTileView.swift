//
//  VideoTile.swift
//  Tiles
//
//  Created by Cotton, Jonathan (Mobile Developer) on 03/02/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit
import AVFoundation

class VideoTileView: TileView {
    override class func layerClass() -> AnyClass {
        return AVPlayerLayer.self
    }
    
    lazy var movementRange: CGFloat = {
        var rand = arc4random_uniform(5)
        if rand < 1 { rand = 1 }
        return CGFloat(rand * 10)
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        if let vLayer = layer as? AVPlayerLayer {
            vLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
            vLayer.masksToBounds = true
        }
        
        clipsToBounds = true
        backgroundColor = UIColor.clearColor()
        titleLabel.backgroundColor = UIColor.blackColor()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.bounds = bounds
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var player: AVPlayer? {
        guard let playerLayer = layer as? AVPlayerLayer
            else { return nil }
        
        if playerLayer.player == nil {
            playerLayer.player = AVPlayer()
        }

        return playerLayer.player
    }
}
