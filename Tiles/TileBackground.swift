//
//  TileBackground.swift
//  Tiles
//
//  Created by Cotton, Jonathan (Mobile Developer) on 31/01/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit

enum TileBackground {
    case solidColour(colour: UIColor)
    case autoGradient(colour: UIColor)
    case gradient(startColour: UIColor, endColour: UIColor)
    case image(path: String)
    case video(path: String)
    case bill(currency: Character, amount: Float)
}